package com.paic.arch.jmsbroker;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

import javax.jms.Connection;
import javax.jms.JMSException;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Created by zhangshu on 2018/3/7.
 * 与具体实现类相关的方法 单独抽出
 * 其他mqBroker 继承 JmsMessageBrokerSupport 即可
 */


public class ActiveJmsMessageBrokerSupport extends JmsMessageBrokerSupport {
    private static final Logger LOG = getLogger(ActiveJmsMessageBrokerSupport.class);
    protected BrokerService brokerService;

    @Override
    void createEmbeddedBroker(String brokerUrl) throws Exception {
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
    }

    @Override
    public Ibroker getInstance(String aBrokerUrl) {
        return this.buildBrokerUrl(aBrokerUrl);
    }

    @Override
    void startEmbeddedBroker(String brokerUrl) throws Exception {
        brokerService.start();
    }

    @Override
    public void stopTheRunningBroker() throws Exception {
        if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
    }

    @Override
    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
    }

    DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, super.brokerUrl));
    }

    @Override
    String executeCallbackAgainstRemoteBroker(String aBrokerUrl, String aDestinationName, JmsCallback aCallback)  {
        Connection connection = null;
        String returnValue = "";
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(aBrokerUrl);
            connection = connectionFactory.createConnection();
            connection.start();
            returnValue = executeCallbackAgainstConnection(connection, aDestinationName, aCallback);
        } catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", aBrokerUrl);
            throw new IllegalStateException(jmse);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close connection to broker at []", aBrokerUrl);
                    throw new IllegalStateException(jmse);
                }
            }
        }
        return returnValue;
    }

}