package com.paic.arch.jmsbroker;

/**
 * Created by zhangshu on 2018/3/7.
 * 之前大量实现代码耦合在一起
 * 再实现一种mq需要重新写代码
 *
 * Ibroker是接口定义
 * activeMq只是实现JmsMessage实现类的一种
 * 抽象出一个接口，让实现类根据不同的实现方法
 * 不同的mq根据此接口取实现相关方法
 * 达到代码复用的目的
 */
public interface Ibroker {
    Ibroker bindToBrokerAtUrl(String REMOTE_BROKER_URL) throws Exception;

    Ibroker andThen();

    Ibroker sendATextMessageToDestinationAt(String queueName,String content);

    String retrieveASingleMessageFromTheDestination(String aDestinationName);

    Ibroker createARunningEmbeddedBrokerOnAvailablePort() throws Exception ;

    void stopTheRunningBroker() throws Exception;

    long getEnqueuedMessageCountAt(String aDestinationName) throws Exception;

    String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout);

    String getBrokerUrl();

    Ibroker getInstance(String aBrokerUrl);

}
