package com.paic.arch.jmsbroker;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.stereotype.Component;

import static org.assertj.core.api.Assertions.assertThat;
@Component
public class JmsMessageBrokerSupportTest {
    /**
     * 通过注入的方式获取broker 解耦调用和实际实现类
     * 这里实际是ActiveJmsMessageBrokerSupport
     * 调整为通过注入的方式注入broker
     * 不同的实现类测试时 修改配置文件即可
     */
    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT = "Lorem blah blah";
    private static Ibroker broker;
    private static String REMOTE_BROKER_URL;
    static{
        ApplicationContext ac = new FileSystemXmlApplicationContext("applicationContext.xml");
        broker = ac.getBean(Ibroker.class);
    }
    @BeforeClass
    public static void setup() throws Exception {
        ApplicationContext ac = new FileSystemXmlApplicationContext("applicationContext.xml");
        broker = (Ibroker) ac.getBean("broker");
        broker.createARunningEmbeddedBrokerOnAvailablePort();
        REMOTE_BROKER_URL = broker.getBrokerUrl();
    }

    @AfterClass
    public static void teardown() throws Exception {
        broker.stopTheRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
        broker.bindToBrokerAtUrl(REMOTE_BROKER_URL) //broker.geturl
                .andThen().sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);
        long messageCount = broker.getEnqueuedMessageCountAt(TEST_QUEUE);
        assertThat(messageCount).isEqualTo(1);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        String receivedMessage = broker.bindToBrokerAtUrl(REMOTE_BROKER_URL)
                .sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
                .andThen().retrieveASingleMessageFromTheDestination(TEST_QUEUE);
        assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
    }

    @Test(expected = JmsMessageBrokerSupport.NoMessageReceivedException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
        broker.bindToBrokerAtUrl(REMOTE_BROKER_URL).retrieveASingleMessageFromTheDestination(TEST_QUEUE, 1);
    }

}